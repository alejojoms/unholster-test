#! /usr/bin/python 

def exixteEnArray(a, array):
	if a in array:
		return True


def tieneCiclos(array):
	index_store = []
	for index in range(len(array)):
		i = index
		j = 0 #esc para buscar ciclos
		while j < len(array):
			try: 
				#print "[",i,"] ",array[i] 
				i = array[i]
				if exixteEnArray(i,index_store):
					return True
				index_store.append(i)
				#print "store ", index_store
				j = j +1
			except:
				index_store = []
				break
	return False


if __name__ == "__main__":
	array = [3,0,4,8,5,9]

	if tieneCiclos(array):
		print "true"
	else:
		print "false"

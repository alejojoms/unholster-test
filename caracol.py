#! /usr/bin/python 

def printCaracol(matriz):
	max_row = len(matriz)
	max_col = len(matriz[0])
	#print "matrix size :", max_row, "x", max_col

	row = 0
	col = 0 

	while (row < max_row) and (col < max_col):
		
		# imprimir la primera columna		
		for i in range(col,max_col):
			print matriz[row][i], 
		# bajamos la primera fila 
		row = row + 1

		#imprimir la ultima fila
		for i in range(row,max_row):
			print matriz[i][max_col-1],
		# columna hacia izquierda
		max_col = max_col - 1

		#imprimir la ultima columna inversa	
		if row < max_row:
			for i in range(max_col-1,col-1,-1):
				print matriz[max_row-1][i],
			max_row = max_row - 1

		#imprimir la primera fila inversa
		if col < max_col:
			for i in range(max_row-1,row-1,-1):
				print matriz[i][col],
			col = col + 1

if __name__ == "__main__":
#	matriz = [[1,2,3],[4,5,6],[7,8,9],[10,11,12]]
#	matriz = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
#	matriz = [[1,2,3,4],[5,6,7,8]]
#	matriz = [[1,2],[3,4],[5,6],[7,8]]
	matriz = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]

#	for i in matriz:
#		print i

	print printCaracol(matriz)


